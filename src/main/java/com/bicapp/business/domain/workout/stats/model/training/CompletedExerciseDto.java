package com.bicapp.business.domain.workout.stats.model.training;

import com.bicapp.business.domain.workout.exercise.model.ExerciseDto;

public record CompletedExerciseDto(Long id, ExerciseDto exercise, int reps) {
    public static CompletedExerciseDto of(CompletedExercise completedExercise) {
        return new CompletedExerciseDto(completedExercise.getId(), ExerciseDto.of(completedExercise.getExercise()),
                completedExercise.getReps());
    }
}
