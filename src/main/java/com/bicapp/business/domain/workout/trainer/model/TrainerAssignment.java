package com.bicapp.business.domain.workout.trainer.model;

import com.bicapp.business.domain.user.model.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
@Entity
public class TrainerAssignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private User trainee;

    @OneToOne
    private User trainer;

    @Enumerated(EnumType.STRING)
    private TrainerAssignmentStatus status;

    @Column
    private String message;

    @JsonProperty("trainee")
    @SuppressWarnings("unused")
    private TrainerDto getTraineeDto() {
        return TrainerDto.of(trainee);
    }

    @JsonProperty("trainer")
    @SuppressWarnings("unused")
    private TrainerDto getTrainerDto() {
        return TrainerDto.of(trainer);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TrainerAssignment that = (TrainerAssignment) o;
        return getId().equals(that.getId()) && getTrainee().equals(that.getTrainee())
                && getTrainer().equals(that.getTrainer()) && getStatus() == that.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTrainee(), getTrainer(), getStatus());
    }
}
