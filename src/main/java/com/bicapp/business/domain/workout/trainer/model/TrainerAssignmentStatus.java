package com.bicapp.business.domain.workout.trainer.model;

public enum TrainerAssignmentStatus {
    ACCEPTED,
    REJECTED,
    PENDING
}
