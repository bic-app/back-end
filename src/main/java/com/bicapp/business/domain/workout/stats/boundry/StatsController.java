package com.bicapp.business.domain.workout.stats.boundry;

import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import com.bicapp.business.domain.workout.stats.control.StatsService;
import com.bicapp.business.domain.workout.stats.model.training.CompletedTraining;
import com.bicapp.business.domain.workout.stats.model.training.CompletedTrainingDto;
import com.bicapp.business.domain.workout.stats.model.training.FatigueLevelDto;
import com.bicapp.business.domain.workout.stats.model.training.TrainingComment;
import com.bicapp.business.domain.workout.stats.model.training.TrainingTypeDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class StatsController {
    private final StatsService statsService;

    @GetMapping("/training-history")
    public List<CompletedTrainingDto> getUsersTrainingHistory(UsernamePasswordAuthentication principal) {
        return statsService.getUsersTrainingHistory(principal);
    }

    @PostMapping("/completed-training")
    public ResponseEntity<?> saveCompletedTraining(UsernamePasswordAuthentication principal,
                                                   @RequestBody CompletedTraining completedTraining,
                                                   UriComponentsBuilder uriComponentsBuilder) {
        CompletedTraining saved = statsService.saveCompletedTraining(principal, completedTraining);
        URI location = uriComponentsBuilder.path("/").build().toUri();
        return ResponseEntity
                .created(location)
                .body(saved);
    }

    @GetMapping("/completed-training")
    public CompletedTrainingDto getCompletedTraining(@RequestParam Long id) {
        return statsService.getCompletedTraining(id);
    }

    @GetMapping("/training-types")
    public List<TrainingTypeDto> getTrainingTypes() {
        return statsService.getTrainingTypes();
    }

    @GetMapping("/fatigue-levels")
    public List<FatigueLevelDto> getFatigueLevels() {
        return statsService.getFatigueLevels();
    }

    @PutMapping("/add-comment")
    public void addComment(@RequestParam Long id, @RequestBody TrainingComment comment) {
        statsService.addComment(id, comment);
    }

    @GetMapping("/feed")
    public List<CompletedTrainingDto> getFeed(UsernamePasswordAuthentication principal) {
        return statsService.getFeed(principal);
    }
}
