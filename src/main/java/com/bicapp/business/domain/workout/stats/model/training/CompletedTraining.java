package com.bicapp.business.domain.workout.stats.model.training;

import com.bicapp.business.domain.user.model.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Getter
@NoArgsConstructor
@Setter
@Entity
@JsonIgnoreProperties(value = {"user"})
public class CompletedTraining {

    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.STRING)
    private TrainingType type;

    @Column
    private LocalDateTime completedAt;

    @Column
    private int timeInMillis;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    List<TrainingComment> comments;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<CompletedExercise> completedExercises;

    @Enumerated(EnumType.STRING)
    private FatigueLevel fatigueLevel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

    public void addComment(TrainingComment comment) {
        comments.add(comment);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompletedTraining that = (CompletedTraining) o;
        return getTimeInMillis() == that.getTimeInMillis() && getId().equals(that.getId())
                && getType() == that.getType() && getCompletedAt().equals(that.getCompletedAt())
                && getComments().equals(that.getComments()) && getCompletedExercises().equals(that.getCompletedExercises())
                && getFatigueLevel() == that.getFatigueLevel() && getUser().equals(that.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getType(), getCompletedAt(), getTimeInMillis(),
                getComments(), getCompletedExercises(), getFatigueLevel(), getUser());
    }
}
