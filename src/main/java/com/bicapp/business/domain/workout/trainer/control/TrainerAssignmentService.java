package com.bicapp.business.domain.workout.trainer.control;

import com.bicapp.business.domain.user.control.UserRepository;
import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.workout.trainer.model.TrainerAssignment;
import com.bicapp.business.domain.workout.trainer.model.TrainerAssignmentStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TrainerAssignmentService {
    private final TrainerAssignmentRepository trainerAssignmentRepository;
    private final UserRepository userRepository;

    public void requestTrainerAssignment(Long trainerId, User user) {
        User trainer = userRepository.findById(trainerId)
                .orElseThrow(() -> new IllegalArgumentException("Trainer not found"));
        trainerAssignmentRepository.findByTraineeAndTrainer(user, trainer)
                .ifPresent(trainerAssignment -> {
                    if (trainerAssignment.getStatus() == TrainerAssignmentStatus.PENDING
                            || trainerAssignment.getStatus() == TrainerAssignmentStatus.ACCEPTED) {
                        throw new IllegalArgumentException("Trainer already requested");
                    }
                });
        TrainerAssignment trainerAssignment = TrainerAssignment.builder()
                .trainer(trainer)
                .trainee(user)
                .status(TrainerAssignmentStatus.PENDING)
                .build();
        trainerAssignmentRepository.save(trainerAssignment);
    }

    public void acceptTrainerAssignment(Long id) {
        trainerAssignmentRepository.findById(id)
                .ifPresent(trainerAssignment -> {
                    if (trainerAssignment.getStatus() != TrainerAssignmentStatus.PENDING) {
                        throw new IllegalArgumentException("Trainer already responded");
                    }
                    trainerAssignment.setStatus(TrainerAssignmentStatus.ACCEPTED);
                    trainerAssignmentRepository.save(trainerAssignment);
                });
    }

    public void rejectTrainerAssignment(Long id) {
        trainerAssignmentRepository.findById(id)
                .ifPresent(trainerAssignment -> {
                    if (trainerAssignment.getStatus() != TrainerAssignmentStatus.PENDING) {
                        throw new IllegalArgumentException("Trainer already responded");
                    }
                    trainerAssignment.setStatus(TrainerAssignmentStatus.REJECTED);
                    trainerAssignmentRepository.save(trainerAssignment);
                });
    }

    public List<TrainerAssignment> getTrainingRequests(User user) {
        return trainerAssignmentRepository.findByTrainerAndStatus(user, TrainerAssignmentStatus.PENDING);
    }
}
