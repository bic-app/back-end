package com.bicapp.business.domain.workout.stats.model.training;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TrainingType {
    RUNNING("Running"),
    CYCLING("Cycling"),
    SWIMMING("Swimming"),
    STRETCHING("Stretching"),
    STRENGTH_TRAINING("Strength Training"),
    HIIT("HIIT"),
    OTHER("Other");
    private final String displayName;
}
