package com.bicapp.business.domain.workout.stats.control;

import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.workout.stats.model.training.CompletedTraining;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompletedTrainingRepository extends JpaRepository<CompletedTraining, Long> {
    List<CompletedTraining> findAllByUser(User user);

    List<CompletedTraining> findAllByUserIn(List<User> users);

}
