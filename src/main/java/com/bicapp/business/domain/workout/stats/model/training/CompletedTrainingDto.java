package com.bicapp.business.domain.workout.stats.model.training;


import java.time.LocalDateTime;
import java.util.List;

public record CompletedTrainingDto(Long id, String type, LocalDateTime completedAt,
                                   int timeInMillis, List<CompletedExerciseDto> completedExercises,
                                   String fatigueLevel, List<TrainingComment> comments, String username) {
    public static CompletedTrainingDto of(CompletedTraining completedTraining) {
        return new CompletedTrainingDto(completedTraining.getId(), completedTraining.getType().getDisplayName(),
                completedTraining.getCompletedAt(), completedTraining.getTimeInMillis(),
                completedTraining.getCompletedExercises().stream().map(CompletedExerciseDto::of).toList(),
                completedTraining.getFatigueLevel().getDisplayName(), completedTraining.getComments(),
                completedTraining.getUser().getUsername());
    }
}
