package com.bicapp.business.domain.workout.stats.control;

import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.workout.stats.model.training.CompletedTraining;
import com.bicapp.business.domain.workout.stats.model.training.CompletedTrainingDto;
import com.bicapp.business.domain.workout.stats.model.training.FatigueLevel;
import com.bicapp.business.domain.workout.stats.model.training.FatigueLevelDto;
import com.bicapp.business.domain.workout.stats.model.training.TrainingComment;
import com.bicapp.business.domain.workout.stats.model.training.TrainingType;
import com.bicapp.business.domain.workout.stats.model.training.TrainingTypeDto;
import com.bicapp.business.domain.workout.trainer.control.TrainerAssignmentRepository;
import com.bicapp.business.domain.workout.trainer.model.TrainerAssignment;
import com.bicapp.business.domain.workout.trainer.model.TrainerAssignmentStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class StatsService {
    private final CompletedTrainingRepository completedTrainingRepository;
    private final TrainerAssignmentRepository trainerAssignmentRepository;

    public CompletedTraining saveCompletedTraining(UsernamePasswordAuthentication principal, CompletedTraining completedTraining) {
        completedTraining.setUser(principal.getUser());
        return completedTrainingRepository.save(completedTraining);
    }

    public List<CompletedTrainingDto> getUsersTrainingHistory(UsernamePasswordAuthentication principal) {
        return completedTrainingRepository.findAllByUser(principal.getUser())
                .stream()
                .map(CompletedTrainingDto::of)
                .toList();
    }

    public CompletedTrainingDto getCompletedTraining(Long id) {
        return completedTrainingRepository.findById(id).map(CompletedTrainingDto::of)
                .orElseThrow(() -> new IllegalArgumentException("Completed training not found"));
    }

    public List<TrainingTypeDto> getTrainingTypes() {
        return Stream.of(TrainingType.values()).map(TrainingTypeDto::of).toList();
    }

    public List<FatigueLevelDto> getFatigueLevels() {
        return Stream.of(FatigueLevel.values()).map(FatigueLevelDto::of).toList();
    }

    public void addComment(Long id, TrainingComment comment) {
        completedTrainingRepository.findById(id)
                .ifPresent(completedTraining -> {
                    completedTraining.addComment(comment);
                    completedTrainingRepository.save(completedTraining);
                });
    }

    public List<CompletedTrainingDto> getFeed(UsernamePasswordAuthentication principal) {
        List<User> trainees = trainerAssignmentRepository.findByTrainerAndStatus(principal.getUser(), TrainerAssignmentStatus.ACCEPTED)
                .stream()
                .map(TrainerAssignment::getTrainee)
                .toList();
        return completedTrainingRepository
                .findAllByUserIn(trainees)
                .stream()
                .map(CompletedTrainingDto::of)
                .toList();
    }
}
