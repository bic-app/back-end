package com.bicapp.business.domain.workout.stats.model.training;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FatigueLevel {
    NONE("None"),
    LIGHT("Light"),
    MEDIUM("Medium"),
    HEAVY("Heavy");
    private final String displayName;
}
