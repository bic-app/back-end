package com.bicapp.business.domain.workout.plan.template.model;

import com.bicapp.business.domain.workout.exercise.model.Exercise;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
public class WorkoutDay {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(joinColumns = {@JoinColumn(name = "workout_day_id")},
            inverseJoinColumns = {@JoinColumn(name = "exercise_id")})
    private List<Exercise> exercises;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkoutDay that = (WorkoutDay) o;
        return getId().equals(that.getId()) && getExercises().equals(that.getExercises());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getExercises());
    }
}
