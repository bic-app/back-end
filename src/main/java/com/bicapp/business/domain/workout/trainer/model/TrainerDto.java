package com.bicapp.business.domain.workout.trainer.model;

import com.bicapp.business.domain.user.model.User;

public record TrainerDto(Long id, String username) {
    public static TrainerDto of(User trainer) {
        return new TrainerDto(trainer.getId(), trainer.getUsername());
    }
}
