package com.bicapp.business.domain.workout.plan.template.model;

public record WorkoutPlanTemplateDto(Long id, String name, String description, String complexity) {
    public static WorkoutPlanTemplateDto from(WorkoutPlanTemplate workoutPlanTemplate) {
        return new WorkoutPlanTemplateDto(workoutPlanTemplate.getId(), workoutPlanTemplate.getName(),
                workoutPlanTemplate.getDescription(), workoutPlanTemplate.getComplexity().getDisplayName());
    }
}
