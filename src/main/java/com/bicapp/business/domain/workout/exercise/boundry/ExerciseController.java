package com.bicapp.business.domain.workout.exercise.boundry;

import com.bicapp.business.domain.workout.exercise.control.ExerciseService;
import com.bicapp.business.domain.workout.exercise.model.ComplexityDto;
import com.bicapp.business.domain.workout.exercise.model.Exercise;
import com.bicapp.business.domain.workout.exercise.model.ExerciseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class ExerciseController {
    private final ExerciseService exerciseService;

    @PostMapping("/exercise")
    public ResponseEntity<Long> createExercise(@RequestBody Exercise exercise, UriComponentsBuilder uriComponentsBuilder) {
        Exercise ex = exerciseService.createExercise(exercise);
        URI location = uriComponentsBuilder.path("/exercise/" + ex.getId()).build().toUri();
        return ResponseEntity
                .created(location)
                .body(ex.getId());
    }

    @GetMapping("/exercise")
    public ExerciseDto createExercise(@RequestParam Long id) {
        return exerciseService.fidExerciseById(id);
    }

    @GetMapping("/exercises")
    public List<Exercise> findAll() {
        return exerciseService.findAll();
    }

    @GetMapping("/exercise/complexity")
    public List<ComplexityDto> getComplexity() {
        return exerciseService.getComplexity();
    }
}
