package com.bicapp.business.domain.workout.stats.model.training;

public record FatigueLevelDto(String value, String displayName) {
    public static FatigueLevelDto of(FatigueLevel fatigueLevel) {
        return new FatigueLevelDto(fatigueLevel.name(), fatigueLevel.getDisplayName());
    }
}
