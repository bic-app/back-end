package com.bicapp.business.domain.workout.trainer.control;

import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.workout.trainer.model.TrainerAssignment;
import com.bicapp.business.domain.workout.trainer.model.TrainerAssignmentStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TrainerAssignmentRepository extends JpaRepository<TrainerAssignment, Long> {
    Optional<TrainerAssignment> findByTraineeAndTrainer(User trainee, User trainer);

    List<TrainerAssignment> findByTrainerAndStatus(User user, TrainerAssignmentStatus pending);

}
