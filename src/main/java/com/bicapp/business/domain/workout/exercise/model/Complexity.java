package com.bicapp.business.domain.workout.exercise.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public enum Complexity {
    VERY_EASY("Very Easy"),
    EASY("Easy"),
    NORMAL("Normal"),
    HARD("Hard"),
    VERY_HARD("Very Hard");
    private final String displayName;
}
