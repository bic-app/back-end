package com.bicapp.business.domain.workout.stats.model.training;

import com.bicapp.business.domain.workout.exercise.model.Exercise;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@RequiredArgsConstructor
@Getter
@Setter
public class CompletedExercise {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(optional = false, cascade = CascadeType.DETACH)
    @JoinColumn(name = "exercise_id", nullable = false, updatable = false, referencedColumnName = "id")
    private Exercise exercise;

    @Column
    private int reps;

    @JsonProperty("exercise_id")
    @SuppressWarnings("unused")
    private void setExerciseId(Long exerciseId) {
        this.exercise = new Exercise();
        exercise.setId(exerciseId);
    }
}
