package com.bicapp.business.domain.workout.exercise.model;

public record ExerciseDto(String name, String description, String complexity, Double duration) {
    public static ExerciseDto of(Exercise exercise) {
        return new ExerciseDto(exercise.getName(), exercise.getDescription(),
                exercise.getComplexity().getDisplayName(), exercise.getDuration());
    }
}
