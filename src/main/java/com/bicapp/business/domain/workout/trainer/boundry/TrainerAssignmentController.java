package com.bicapp.business.domain.workout.trainer.boundry;

import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import com.bicapp.business.domain.workout.trainer.control.TrainerAssignmentService;
import com.bicapp.business.domain.workout.trainer.model.TrainerAssignment;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class TrainerAssignmentController {
    private final TrainerAssignmentService trainerAssignmentService;

    @GetMapping("/request-trainer")
    public void requestTrainerAssignment(@RequestParam Long trainerId, UsernamePasswordAuthentication principal) {
        trainerAssignmentService.requestTrainerAssignment(trainerId, principal.getUser());
    }

    @PutMapping("/accept-trainee/{id}")
    public void acceptTrainerAssignment(@PathVariable Long id) {
        trainerAssignmentService.acceptTrainerAssignment(id);
    }

    @PutMapping("/reject-trainee/{id}")
    public void rejectTrainerAssignment(@PathVariable Long id) {
        trainerAssignmentService.rejectTrainerAssignment(id);
    }

    @GetMapping("/training-requests")
    public List<TrainerAssignment> getTrainingRequests(UsernamePasswordAuthentication principal) {
        return trainerAssignmentService.getTrainingRequests(principal.getUser());
    }
}
