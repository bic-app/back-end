package com.bicapp.business.domain.workout.stats.model.training;

public record TrainingTypeDto(String value, String displayName) {
    public static TrainingTypeDto of(TrainingType trainingType) {
        return new TrainingTypeDto(trainingType.name(), trainingType.getDisplayName());
    }
}
