package com.bicapp.business.domain.workout.plan.template.boundry;

import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import com.bicapp.business.domain.workout.plan.template.control.WorkoutPlanTemplateService;
import com.bicapp.business.domain.workout.plan.template.model.WorkoutPlanTemplate;
import com.bicapp.business.domain.workout.plan.template.model.WorkoutPlanTemplateDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RequestMapping("/workout-plan-template")
@RestController
@RequiredArgsConstructor
public class WorkoutPlanTemplateController {
    private final WorkoutPlanTemplateService workoutPlanTemplateService;

    @PostMapping("")
    public ResponseEntity<?> createWorkoutPlanTemplate(@RequestBody WorkoutPlanTemplate workoutPlanTemplate,
                                                       UriComponentsBuilder uriComponentsBuilder) {
        workoutPlanTemplateService.createWorkoutPlanTemplate(workoutPlanTemplate);
        URI location = uriComponentsBuilder.path("/").build().toUri();
        return ResponseEntity
                .created(location)
                .build();
    }

    @GetMapping("")
    public List<WorkoutPlanTemplateDto> findAllWorkoutPlanTemplate() {
        return workoutPlanTemplateService.findAllWorkoutPlanTemplate();
    }

    @GetMapping("/mine")
    public List<WorkoutPlanTemplateDto> findAllWorkoutPlanTemplate(UsernamePasswordAuthentication principal) {
        return workoutPlanTemplateService.findAllWorkoutPlanTemplatesForLoggedInUser(principal);
    }

    @GetMapping("/{id}")
    public WorkoutPlanTemplate findWorkoutPlanTemplate(@PathVariable("id") Long id) {
        return workoutPlanTemplateService.findWorkoutPlanTemplateById(id);
    }

    @PostMapping("/mine")
    public ResponseEntity<?> assignWorkoutPlanTemplateToLoggedInUser(UsernamePasswordAuthentication principal,
                                                                     @RequestBody WorkoutPlanTemplate workoutPlanTemplate,
                                                                     UriComponentsBuilder uriComponentsBuilder) {
        workoutPlanTemplateService.assignWorkoutPlanTemplateToLoggedInUser(principal, workoutPlanTemplate);
        URI location = uriComponentsBuilder.path("/").build().toUri();
        return ResponseEntity
                .created(location)
                .build();
    }
}
