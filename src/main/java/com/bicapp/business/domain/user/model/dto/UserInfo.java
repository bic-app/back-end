package com.bicapp.business.domain.user.model.dto;

import com.bicapp.business.domain.user.model.User;

import java.util.List;

public record UserInfo(Long id, String username, Boolean enabled, List<String> authorities) {
    public static UserInfo of(User user) {
        return new UserInfo(user.getId(), user.getUsername(), user.isEnabled(), user.getAuthorities().stream()
                .map(a -> a.getName().getDisplayName()).toList());
    }
}
