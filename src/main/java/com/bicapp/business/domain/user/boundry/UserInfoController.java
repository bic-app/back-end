package com.bicapp.business.domain.user.boundry;

import com.bicapp.business.domain.user.control.UserService;
import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.user.model.dto.UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class UserInfoController {
    private final UserService userService;

    @GetMapping("/user-info")
    public ResponseEntity<UserInfo> getUserInfo(UsernamePasswordAuthentication principal) {
        User user = principal.getUser();
        UserInfo userInfo = UserInfo.of(user);
        return ResponseEntity.ok(userInfo);
    }

    @GetMapping("/trainers")
    public ResponseEntity<List<User>> getTrainers(UsernamePasswordAuthentication principal) {
        return ResponseEntity.ok(userService.getTrainers(principal.getUser()));
    }

}
