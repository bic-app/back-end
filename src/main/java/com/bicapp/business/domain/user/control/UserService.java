package com.bicapp.business.domain.user.control;

import com.bicapp.business.domain.user.authorities.model.AuthorityEnum;
import com.bicapp.business.domain.user.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public List<User> getTrainers(User user) {
        return userRepository.findAllUsersWithAuthorityAndNotUser(AuthorityEnum.TRAINER, user);
    }
}
