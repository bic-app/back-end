package com.bicapp.business.domain.user.authorities.control;

import com.bicapp.business.domain.user.authorities.model.Authority;
import com.bicapp.business.domain.user.authorities.model.AuthorityEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Optional<Authority> findAuthorityByName(AuthorityEnum name);
}
