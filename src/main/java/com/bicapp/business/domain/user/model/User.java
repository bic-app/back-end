package com.bicapp.business.domain.user.model;

import com.bicapp.business.domain.user.authorities.model.Authority;
import com.bicapp.business.domain.user.authorities.model.AuthorityEnum;
import com.bicapp.business.domain.workout.plan.template.model.WorkoutPlanTemplate;
import com.bicapp.business.domain.workout.stats.model.training.CompletedTraining;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Objects;

@Entity
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column
    private boolean enabled;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "User_Authority",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "authority_id")})
    private List<Authority> authorities;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "workout_plan_templates_id")})
    private List<WorkoutPlanTemplate> workoutPlanTemplates;

    @OneToMany(fetch = FetchType.LAZY)
    private List<CompletedTraining> completedTrainings;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return getId().equals(user.getId()) && getUsername().equals(user.getUsername())
                && getPassword().equals(user.getPassword()) && getAuthorities().equals(user.getAuthorities());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUsername(), getPassword(), getAuthorities());
    }

    public boolean isAdmin() {
        return authorities.stream().anyMatch(a -> a.getName().equals(AuthorityEnum.ADMIN));
    }
}
