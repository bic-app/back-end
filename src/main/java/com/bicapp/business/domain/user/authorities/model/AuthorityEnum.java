package com.bicapp.business.domain.user.authorities.model;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public enum AuthorityEnum {
    TRAINER("Trainer"),
    TRAINEE("Trainee"),
    TRAINING_FACILITY_ADMIN("Training Facility Admin"),
    ADMIN("Administrator");
    private final String displayName;

    AuthorityEnum(String displayName) {
        this.displayName = displayName;
    }
}
