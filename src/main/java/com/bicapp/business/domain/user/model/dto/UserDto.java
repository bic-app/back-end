package com.bicapp.business.domain.user.model.dto;

import com.bicapp.business.domain.user.authorities.model.AuthorityEnum;

import java.util.List;

public record UserDto(String username, String password, List<AuthorityEnum> authority) {
}
