package com.bicapp.business.domain.auth.boundry;

import com.bicapp.business.domain.auth.control.AuthenticationService;
import com.bicapp.business.domain.user.model.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@RestController
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserDto user, HttpServletResponse response) {
        String jwt = authenticationService.authenticate(user);
        return authenticationService.authorizedResponse(response, jwt);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserDto userDto, HttpServletResponse response) {
        String jwt = authenticationService.createUser(userDto);
        return authenticationService.authorizedResponse(response, jwt);
    }

    @PostMapping("/sign-out")
    public ResponseEntity<?> signOut(HttpServletResponse response) {
        return authenticationService.expireAuthCookie(response, null);
    }
}
