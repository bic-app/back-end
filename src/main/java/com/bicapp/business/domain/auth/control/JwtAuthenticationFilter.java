package com.bicapp.business.domain.auth.control;

import com.bicapp.business.domain.auth.OpenEndpoints;
import com.bicapp.business.domain.user.control.JpaUserDetailsService;
import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.crypto.SecretKey;
import javax.security.sasl.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JpaUserDetailsService userDetailsService;
    @Value("${jwt.signing.key}")
    private String signingKey;

    @Autowired
    public JwtAuthenticationFilter(@Lazy JpaUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String jwt = getJwt(request);
        SecretKey key = Keys.hmacShaKeyFor(signingKey.getBytes(StandardCharsets.UTF_8));
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(jwt)
                .getBody();
        String username = String.valueOf(claims.get("username"));
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        var auth = new UsernamePasswordAuthentication(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

        filterChain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        String servletPath = request.getServletPath();
        return OpenEndpoints.stream()
                .map(OpenEndpoints::getUrl)
                .anyMatch(servletPath::startsWith);
    }

    private String getJwt(HttpServletRequest request) throws AuthenticationException {
        Cookie[] cookies = Optional.ofNullable(request.getCookies())
                .orElseThrow(AuthenticationException::new);
        return Arrays.stream(cookies)
                .filter(c -> c.getName().equals("auth-token"))
                .map(Cookie::getValue)
                .findFirst()
                .orElseThrow(AuthenticationException::new);
    }
}
