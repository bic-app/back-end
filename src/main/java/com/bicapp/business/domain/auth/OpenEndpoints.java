package com.bicapp.business.domain.auth;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
public enum OpenEndpoints {
    LOGIN("/login"),
    REGISTER("/register"),
    SIGN_OUT("/sign-out"),
    ACTUATOR("/actuator"),
    H2_CONSOLE("/h2-console"),
    DOC("/api/doc"),
    V3_API_DOCS("/v3/api-docs"),
    SWAGGER("/swagger-ui");

    private final String url;

    public static Stream<OpenEndpoints> stream() {
        return Stream.of(OpenEndpoints.values());
    }
}
