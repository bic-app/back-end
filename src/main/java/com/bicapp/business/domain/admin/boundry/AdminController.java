package com.bicapp.business.domain.admin.boundry;

import com.bicapp.business.domain.admin.control.AdminService;
import com.bicapp.business.domain.user.control.UsernamePasswordAuthentication;
import com.bicapp.business.domain.user.model.dto.UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AdminController {
    private final AdminService adminService;

    @PostMapping("/admin/user")
    public void banUser(@RequestBody UserInfo userInfo, UsernamePasswordAuthentication principal) {
        adminService.updateUser(userInfo, principal.getUser());
    }

    @DeleteMapping("/admin/user")
    public void deleteUser(@RequestParam Long id) {
        adminService.deleteUser(id);
    }

    @GetMapping("/admin/users")
    public List<UserInfo> getAllUsers(UsernamePasswordAuthentication principal) {
        return adminService.getAllUsers(principal.getUser());
    }
}
