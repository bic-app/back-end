package com.bicapp.business.domain.admin.control;

import com.bicapp.business.domain.user.control.UserRepository;
import com.bicapp.business.domain.user.model.User;
import com.bicapp.business.domain.user.model.dto.UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AdminService {
    private final UserRepository userRepository;

    public void deleteUser(Long id) {
        userRepository.findById(id).ifPresent(userRepository::delete);
    }

    public void updateUser(UserInfo userInfo, User user) {
        if (!user.isAdmin()) {
            throw new IllegalArgumentException("User is not admin");
        }
        userRepository.findById(userInfo.id()).ifPresent(u -> {
            u.setEnabled(userInfo.enabled());
            userRepository.save(u);
        });
    }

    public List<UserInfo> getAllUsers(User user) {
        if (user.isAdmin()) {
            return userRepository.findAll().stream()
                    .filter(u -> !u.getId().equals(user.getId()))
                    .map(UserInfo::of).toList();
        } else {
            throw new IllegalArgumentException("User is not admin");
        }
    }
}