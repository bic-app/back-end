package com.bicapp;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.ApplicationPidFileWriter;

@SpringBootApplication
public class BicappApplication {
    public static void main(String[] args) {
        SpringApplicationBuilder app = new SpringApplicationBuilder(BicappApplication.class)
                .web(WebApplicationType.SERVLET);
        app.build().addListeners(new ApplicationPidFileWriter("/webapps/bicapp/bin/shutdown.pid"));
        app.run();
    }
}
