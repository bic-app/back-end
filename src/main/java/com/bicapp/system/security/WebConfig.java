package com.bicapp.system.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:8080", "https://bic-app.gitlab.io", "http://bic-app.gitlab.io")
                .allowedMethods("GET", "POST", "PUT", "DELETE")
                .exposedHeaders("Location")
                .allowCredentials(true);
    }
}
