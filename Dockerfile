FROM amazoncorretto:17
VOLUME /tmp
ADD /public/bicapp.jar /bicapp.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/bicapp.jar"]